MuZIEum App
-----------

An app based on LingQuest. 
Will be displayed in the MuZIEum. 
Should be a game that is easy to use for visually impaired. 
The player is presented audio clips in which people speak in several languages. 
The goal is to pick the clip containing the same language as the target audio clip. 