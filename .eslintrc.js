module.exports = {
    "installedESLint": true,

    "extends": ["eslint:recommended", "plugin:react/recommended"],

    "plugins": [
        "react"
    ],

    "parserOptions": {
        "ecmaVersion": 6,
        "sourceType": "module",
        "ecmaFeatures": {
            "impliedStrict": true,
            "jsx": true,
            "experimentalObjectRestSpread": true,
        }
    },

    "env": {
        "browser": true
    },

    "rules": {
        "no-console": "off",
    }

};