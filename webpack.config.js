
var webpack = require('webpack');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var process = require('process');


module.exports = {
    entry: [
        //'webpack-dev-server/client?http://localhost:8080', // WebpackDevServer host and port
        //'webpack/hot/only-dev-server', // "only" prevents reload on syntax errors
        './src/index.js',
    ],
    output: {
        path: './build',
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loaders: ['react-hot', 'babel-loader'],
            },
            {
                test: /\.json$/,
                exclude: /node_modules/,
                loaders: ['json'],
            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                loaders: ['style', 'css'],
            },
            {
                test: /\.less$/,
                exclude: /node_modules/,
                loaders: ['style', 'css', 'less'],
            },
            {   
                test: /\.html?$|\.jpe?g$|\.gif$|\.png$|\.svg(\?.*)?$|\.woff(\?.*)?$|\.woff2(\?.*)?$|\.ttf(\?.*)?$|\.eot(\?.*)?$/,
                exclude: /node_modules/,
                loader: "file-loader?name=[path][name].[ext]&context=src"
            }
        ]
    },
    plugins: [
        //new webpack.HotModuleReplacementPlugin(),

        //new CopyWebpackPlugin([
        //    { from: './src/index.html', to: 'index.html' },
        //]),

        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
        }),
    ]
};