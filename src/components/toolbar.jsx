
import React from 'react';

import { connect } from 'react-redux';

import { changePage, changeLang } from '../common/actions';
import { PAGES, GAME_STAGES, LANGS } from '../common/constants';

import { tr } from '../common/translations';

var Toolbar = React.createClass({
    render: function() {
        let page = this.props.page;
        let title = '';
        let lang = this.props.lang;
        
        if (page === PAGES.GAME) {
            let stage = this.props.game.stage;
            if (this.props.game.stage === GAME_STAGES.QUESTION) {
                title = tr(lang, 'TB.Round') + ' ' + (this.props.game.currentQuestionId+1);
            } else if (stage === GAME_STAGES.ANSWER) {
                title = tr(lang, 'TB.Answer');
            } else if (stage === GAME_STAGES.INFO) {
                title = tr(lang, 'TB.Info');
            } else if (stage === GAME_STAGES.ENDING) {
                title = tr(lang, 'TB.End');
            } else if (stage === GAME_STAGES.INTRO) {
                title = tr(lang, 'TB.Introduction');
            }
        } else {
            if (page === PAGES.ABOUT) {
                title = tr(lang, 'TB.About');
            }
            if (page === PAGES.SETTINGS) {
                title = tr(lang, 'TB.Settings');
            }
            if (page === PAGES.HOME) {
                title = tr(lang, 'TB.Home');
            }
        }

        let otherLang = this.props.lang === LANGS.NL ? LANGS.EN : LANGS.NL;

        return (
            <div id="toolbar">
                <div className="home button" role="button" aria-label={tr(lang, 'TB.HomeBtn')} tabIndex="1" onClick={()=>this.props.changePage(PAGES.GAME)}>
                    <i className="fa fa-home" aria-hidden="true"></i>
                </div>
                <div className="title">{title}</div>
                <div className="lang button" role="button" aria-label={tr(lang, 'TB.ChangeLanguageBtn')} tabIndex="2" onClick={()=>this.props.changeLang(otherLang)}>
                    <span className="lang-text">{lang}</span><i className="fa fa-flag" aria-hidden="true"></i>
                </div>
                <div className="settings button" role="button" aria-label={tr(lang, 'TB.AboutBtn')} tabIndex="2" onClick={()=>this.props.changePage(PAGES.ABOUT)}>
                    <i className="fa fa-info-circle" aria-hidden="true"></i>
                </div>
            </div>
        );
    }
});

var ConnectedToolbar = connect(
    (state) => {
        return state;
    },
    { changePage, changeLang }
)(Toolbar);

export { ConnectedToolbar as default, Toolbar };