
import React, { PropTypes } from 'react';

import Toolbar from './toolbar.jsx';

import { connect } from 'react-redux';
import classNames from 'classnames';


import { PAGES, GAME_STAGES, LANGS } from '../common/constants';
import { changePage, answerQuestion, changeQuestion, changeGameStage } from '../common/actions';

import { tr } from '../common/translations';


const SOUNDS = Object.freeze({
    GOOD: './media/good.wav',
    BAD: './media/bad.wav',
    END: './media/end.wav',
});


function pad(paddingValue, str) {
    return String(paddingValue + str).slice(-paddingValue.length);
}

const ANSWER_ICONS = Object.freeze({
    GOOD: 'GOOD',
    BAD: 'BAD',
    SPACER: 'SPACER',
});


// An element with controls for an audio file with play/pause button, progress indicator, and a label and icon
// Complete with aria-attributes
const AudioItem = React.createClass({
    propTypes: {
        className: PropTypes.string,
        filePath: PropTypes.string.isRequired,
        label:  PropTypes.string.isRequired,
        ariaPlayLabel: PropTypes.string.isRequired,
        chooseFn: PropTypes.func,
        chooseText: PropTypes.string,
        ariaChooseLabel: PropTypes.string,
        icon: PropTypes.string,
        onBeforePlay: PropTypes.func,
    },

    getInitialState: function() {
        return {
            isPlaying: false,
            duration: 0,
            currentTime: 0,
        }
    },

    componentDidMount: function() {
        if (this._player) {
            this._player.addEventListener('timeupdate', this._updatePlayerState);
            this._player.addEventListener("loadedmetadata", this._updatePlayerState);
            this._player.addEventListener("play", this._updatePlayerState);
            this._player.addEventListener("pause", this._updatePlayerState);
        }
    },

    componentWillUnmount: function() {
        if (this._player) {
            this._player.removeEventListener('timeupdate', this._updatePlayerState);
            this._player.removeEventListener("loadedmetadata", this._updatePlayerState);
            this._player.removeEventListener("play", this._updatePlayerState);
            this._player.removeEventListener("pause", this._updatePlayerState);
        }
    },

    _player: null,

    _updatePlayerState: function() {
        if (this._player) {
            this.setState({
                isPlaying: !(this._player.paused || this._player.ended),
                duration: this._player.duration,
                currentTime: this._player.currentTime, 
            });
        }
    },

    // Called externally by AudioCollection
    pause: function() {
        if (this._player) {
            this._player.pause();
        }
    },

    // Called externally by AudioCollection
    focus: function() {
        if (this.refs.player) {
            // Note: can only focus if it has a tabIndex >= 0
            this.refs.player.focus();
        }
    },

    _togglePlay: function(e) {
        if (!this._player) {
            return;
        }

        e.preventDefault();
        e.stopPropagation();

        if (this._player.paused || this._player.ended) {
            if (this.props.onBeforePlay) {
                this.props.onBeforePlay();
            }
            this._player.play();
        } else {
            this._player.pause();
        }
    },

    _timeString: function() {
        var curSec = pad("00", Math.floor(this.state.currentTime%60));
        var curMin = pad("00", Math.floor(this.state.currentTime/60));
        var durSec = pad("00", Math.floor(this.state.duration%60));
        var durMin = pad("00", Math.floor(this.state.duration/60));
        return curMin+":"+curSec+" / "+durMin+":"+durSec;
    },

    _renderIcon: function() {
        let icon;
        if (this.props.icon) {
            if (this.props.icon === ANSWER_ICONS.GOOD) {
                icon = <div className="icon"><i className="fa fa-check fa-fw" aria-hidden="true"></i></div>
            } else if (this.props.icon === ANSWER_ICONS.BAD) {
                icon = <div className="icon"><i className="fa fa-times fa-fw" aria-hidden="true"></i></div>
            } else if (this.props.icon === ANSWER_ICONS.SPACER){
                icon = <div className="icon"><i className="fa fa-fw" aria-hidden="true"></i></div> 
            }
        }
        return icon;
    },

    _renderButton: function() {
        let button;
        if (this.props.chooseFn) {
            let handleButtonClick = (e) => {
                e.preventDefault();
                e.stopPropagation();
                this.props.chooseFn(e);
            }

            button = (
                <div className="button" onClick={handleButtonClick} role="button" tabIndex={0} aria-label={this.props.ariaChooseLabel}>{this.props.chooseText}</div>
            );
        }
        return button;
    },

    render: function() {

        let audioItemCn = classNames(
            this.props.className, {
            'audio-item': true,
            '-playing': this.state.isPlaying,
        });

        let playIcon = <i className="fa fa-play-circle fa-2x"></i>
        if (this.state.isPlaying) {
            playIcon = <i className="fa fa-pause-circle fa-2x"></i>;
        }

        let playRatio = this.state.currentTime / this.state.duration;

        return (
            <div className={audioItemCn} onClick={this._togglePlay}>
                <div className="player" ref="player" role="button" tabIndex={0} aria-label={this.props.ariaPlayLabel} >
                    <audio 
                        src={this.props.filePath} 
                        ref={(ref)=>{this._player = ref}}></audio>
                    <div className="name">{this.props.label}</div>
                    <div className="controller" onClick={this._togglePlay}>{playIcon}</div>
                    <div className="bar" aria-hidden="true"><div className="filler" style={{width:(playRatio*100)+'%'}}></div></div>
                    <div className="time" aria-hidden="true">{this._timeString()}</div>
                    {this._renderIcon()}
                </div>
                {this._renderButton()}
            </div>
        );
    }
});



function audioClipFilePath(langId, clipName) {
    return 'clips/' + langId + '/s' + clipName + '.m4a';
}

function optionIdToLetter(optionId) {
    let letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    if (Number.isInteger(optionId) && optionId < letters.length) {
        return letters[optionId];
    }
    return '?';
}

function personName(lang, optionId = null) {
    if (optionId === null) {
        return tr(lang, 'AC.Traveler');
    } else {
        return tr(lang, 'AC.PersonX', {x:optionIdToLetter(optionId)});
    }
}

function langName(lang, languages, langId) {
    let propName = 'name_'+lang;
    if (languages[langId] && languages[langId][propName]) {
        return languages[langId][propName];
    } else {
        return '?';
    }
}

// Contains a collection of audio items, one "target" audio item, and several "options" audio items which can be chosen
// Controls behaviour for choosing, label names, and color styling and icons if an option has been chosen
let AudioCollection = React.createClass({
    propTypes: {
        lang: PropTypes.string.isRequired,
        target: PropTypes.object.isRequired,
        options: PropTypes.array.isRequired,
        chosenOptionId: PropTypes.number,
        languages: PropTypes.object,
        makeChooseFn: PropTypes.func,
    },

    componentDidMount: function() {
        if (this.props.chosenOptionId === undefined) {
            let ref = this._audioItemRef();
            if (this.refs[ref]) {
                this.refs[ref].focus();
            }
        }
    },

    _pauseAll: function() {
        if (this.refs.target && this.refs.target.pause) {
            this.refs.target.pause();
        }
        for (var i=0; i < 3; i++) {
            let index = 'option'+i;
            if (this.refs[index] && this.refs[index].pause) {
                this.refs[index].pause();
            }
        }
    },

    _audioItemRef: function(optionId = null) {
        if (optionId !== null) {
            return 'option'+optionId;
        } else {
            return 'target';
        }
    },

    _renderAudioItem: function(item, optionId = null) {
        let lang = this.props.lang;
        let questionIsAnswered = this.props.chosenOptionId !== undefined;
        let isOption = optionId !== null;
        let isCorrect = isOption && (this.props.target.langId === item.langId);
        let isChosen = isOption && (optionId === this.props.chosenOptionId);

        let label, ariaPlayLabel;
        if (questionIsAnswered) {
            label = langName(lang, this.props.languages, item.langId);
            ariaPlayLabel = tr(lang, 'AC.ListenToXSpeakingY', {x: personName(lang, optionId), y: label});
        } else {
            label = personName(lang, optionId);
            ariaPlayLabel = tr(lang, 'AC.ListenToX', {x: personName(lang, optionId)});
        }

        let cn, icon;
        if (questionIsAnswered) {
            cn = classNames({
                '-correct': isCorrect,
                '-incorrect': !isCorrect,
                '-chosen': isChosen,
            });

            if (isCorrect) {
                icon = ANSWER_ICONS.GOOD;
            } else if (isChosen) {
                icon = ANSWER_ICONS.BAD;
            } else {
                icon = ANSWER_ICONS.SPACER; // Hidden icon for equal spacing 
            }
        }

        let chooseFn, ariaChooseLabel;
        if (!questionIsAnswered && isOption) {
            chooseFn = this.props.makeChooseFn(optionId);
            ariaChooseLabel = tr(lang, 'AC.ChooseX', {x: personName(lang, optionId)});
        }
        
        return (
            <AudioItem 
                key={optionId}
                ref={this._audioItemRef(optionId)}
                className={cn}
                filePath={audioClipFilePath(item.langId, item.clipName)} 
                label={label}
                ariaPlayLabel={ariaPlayLabel}
                ariaChooseLabel={ariaChooseLabel}
                icon={icon}
                chooseFn={chooseFn}
                chooseText={tr(lang, 'AC.Choose')}
                onBeforePlay={this._pauseAll} />
        );
        
    },

    render: function() {
        return (
            <div role="group">
                {this._renderAudioItem(this.props.target)}
                <div className="line" />
                {this.props.options.map((option, index)=>this._renderAudioItem(option, index))}
            </div>
        );
    }
});


// Content page showing an answered question
let Answer = React.createClass({
    propTypes: {
        lang: PropTypes.string.isRequired,
        isCorrect: PropTypes.bool.isRequired,
        target: PropTypes.object.isRequired,
        options: PropTypes.array.isRequired,
        chosenOptionId: PropTypes.number.isRequired,
        languages: PropTypes.object.isRequired,
        changeGameStage: PropTypes.func.isRequired,
        playGoodSound: PropTypes.func.isRequired,
        playBadSound: PropTypes.func.isRequired,
    },

    componentDidMount: function() {
        // Play good/bad sound effect onload (audio autoplay doesn't work on mobile browsers)
        if (this.props.isCorrect) {
            this.props.playGoodSound();
        } else {
            this.props.playBadSound();
        }
    },

    _continue: function() {
        return this.props.changeGameStage(GAME_STAGES.INFO);
    },

    render: function() {
        let lang = this.props.lang;
        let chosenLetter = optionIdToLetter(this.props.chosenOptionId);
        let correctLang = langName(lang, this.props.languages, this.props.target.langId);
        let chosenLang = langName(lang, this.props.languages, this.props.options[this.props.chosenOptionId].langId);

        let title, result;
        if (this.props.isCorrect) {
            title = tr(lang, 'ANS.Correct');
            result = tr(lang, 'ANS.CorrectExplain', {chosenLetter, correctLang});
        } else {
            title = tr(lang, 'ANS.Incorrect');
            result = tr(lang, 'ANS.IncorrectExplain', {chosenLetter, chosenLang, correctLang});
        }

        let titleCn = classNames({
            correct: this.props.isCorrect,
            incorrect: !this.props.isCorrect,
        });

        return (
            <div className="main">
                <div className="content divided">
                    <div className="left">
                        <img src="./media/airport.png" aria-hidden="true" />
                    </div>
                    <div className="right">
                        <div className="top">
                            <h1 className={titleCn} id="test">{title}</h1>
                            <p className="main-text" aria-labelledby="test">{result}</p>
                        </div>
                        <div className="bot">
                            <AudioCollection 
                                lang={this.props.lang}
                                target={this.props.target} 
                                options={this.props.options} 
                                languages={this.props.languages}
                                chosenOptionId={this.props.chosenOptionId} />
                        </div>
                    </div>
                </div>
                <BotNavBar lang={this.props.lang} nextFn={this._continue} nextFocus={true} />
            </div>
        )
    }
});

var ConnectedAnswer = connect(
    (state) => {
        let question = state.questions[state.game.currentQuestionId];
        let chosenOptionId = question.chosenOptionId;
        let target = question.target;
        let options = question.options;
        let isCorrect = options[chosenOptionId].langId === target.langId;

        return {
            lang: state.lang,
            isCorrect: isCorrect,
            target: target,
            options: options,
            chosenOptionId: chosenOptionId,
            languages: state.languages,
        };
    },
    { changeGameStage }
)(Answer);


// Content page showing an unanswered question
var Question = React.createClass({
    propTypes: {
        lang: PropTypes.string.isRequired,
        questionId: PropTypes.number.isRequired,
        target: PropTypes.object.isRequired,
        options: PropTypes.array.isRequired,
        answerQuestion: PropTypes.func.isRequired,
        changeGameStage: PropTypes.func.isRequired,
    },

    _makeChooseFn: function(index) {
        return () => {
            this.props.answerQuestion(this.props.questionId, index);
            this.props.changeGameStage(GAME_STAGES.ANSWER);
        }
    },

    render: function() {
        let lang = this.props.lang;
        return (
            <div className="main">
                <div className="content divided">
                    <div className="left">
                        <img src="./media/airport.png" aria-hidden="true" />
                    </div>
                    <div className="right">
                        <div className="top">
                            <h1>{tr(lang, 'Q.Title')}</h1>
                            <p className="main-text">
                                {tr(lang, 'Q.Instruction')}
                            </p>
                        </div>
                        <div className="bot">
                            <AudioCollection 
                                lang={this.props.lang}
                                target={this.props.target} 
                                options={this.props.options} 
                                makeChooseFn={this._makeChooseFn} />
                        </div>
                    </div>
                </div>
                <BotNavBar lang={this.props.lang} />
            </div>
        )
    }
});

var ConnectedQuestion = connect(
    (state) => {
        let question = state.questions[state.game.currentQuestionId];
        return {
            lang: state.lang,
            questionId: state.game.currentQuestionId,
            target: question.target,
            options: question.options,
        };
    },
    { answerQuestion, changeGameStage }
)(Question);


// Top progress bar with a line and number of answered and correct questions
let ProgressBar = React.createClass({
    propTypes: {
        lang: PropTypes.string.isRequired,
        nTotal: PropTypes.number.isRequired,
        nAnswered: PropTypes.number.isRequired,
        nCorrect: PropTypes.number.isRequired,
    },

    render: function() {
        let percentage = Math.round((this.props.nAnswered/this.props.nTotal)*100);

        let fillerStyle = {
            width: percentage+'%',
        };

        let lang = this.props.lang;

        return (
            <div className="progress" aria-hidden="true">
                <div className="round text">{tr(lang, 'PB.Answered')}: {this.props.nAnswered}/{this.props.nTotal}</div>
                <div className="bar">
                    <div className="fill" style={fillerStyle}></div>
                </div>
                <div className="score text">{tr(lang, 'PB.Correct')}: {this.props.nCorrect}/{this.props.nTotal}</div>
            </div>
        );
    }
});

var ConnectedProgressBar = connect(
    (state) => {
        return {
            lang: state.lang,
            nTotal: state.questions.length,
            nAnswered: state.game.nAnswered,
            nCorrect: state.game.nCorrect,
        };
    },
    {  }
)(ProgressBar);


// Bottom navigation bar with forward and possibly back buttons
let BotNavBar = React.createClass({
    propTypes: {
        lang: PropTypes.string.isRequired,
        nextFn: PropTypes.func,
        prevFn: PropTypes.func,
        nextFocus: PropTypes.bool,
    },

    componentDidMount: function() {
        if (this.props.nextFocus) {
            if (this.refs["next"]) {
                this.refs["next"].focus();
            }
        }
    },

    render: function() {
        let prevCn = classNames({
            'button': true,
            '-hidden': this.props.prevFn == undefined,
        });
        let nextCn = classNames({
            'button': true,
            '-hidden': this.props.nextFn == undefined,
        });

        let prevAriaHidden = 'false';
        let prevTabIndex = 0;
        if (this.props.prevFn == undefined) {
            prevAriaHidden = 'true';
            prevTabIndex = -1;
        }

        let nextAriaHidden = 'false';
        let nextTabIndex = 0;
        if (this.props.nextFn == undefined) {
            nextAriaHidden = 'true';
            nextTabIndex = -1;
        }

        let lang = this.props.lang;

        return (
            <div className="bot-nav-bar">
                <div className="left">
                    <div className={prevCn} onClick={this.props.prevFn} role="button" aria-hidden={prevAriaHidden} tabIndex={prevTabIndex}>
                        {tr(lang, 'BOT.Back')}
                    </div>
                </div>
                <div className="right">
                    <div className={nextCn} onClick={this.props.nextFn} ref="next" role="button" aria-hidden={nextAriaHidden} tabIndex={nextTabIndex}>
                        {tr(lang, 'BOT.Continue')}
                    </div>
                </div>
            </div>
        )
    }
});








// Content page showing background information for an answered question
let Info = React.createClass({
    propTypes: {
        lang: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        population: PropTypes.number.isRequired,
        family: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,

        questionId: PropTypes.number.isRequired,
        nQuestions: PropTypes.number.isRequired,
        changeGameStage: PropTypes.func.isRequired,
        changeQuestion: PropTypes.func.isRequired,
    },

    _continue: function() {
        if (this.props.questionId+1 >= this.props.nQuestions) {
            this.props.changeGameStage(GAME_STAGES.ENDING);
        } else {
            this.props.changeGameStage(GAME_STAGES.QUESTION);
            this.props.changeQuestion(this.props.questionId+1);
        }
    },

    _formatPopulation: function(lang, pop) {
        var str = String(pop);
        if (pop > 9999) {
            str = ""+Math.round(pop/1000) + ' ' + tr(lang, 'INF.thousand');
        }
        if (pop > 1000000) {
            str = ""+Math.round(pop/1000000) + ' ' + tr(lang, 'INF.million');
        }
        return str;
    },

    render: function() {
        let lang = this.props.lang;
        return (
            <div className="main">
                <div className="content divided">
                    <div className="left">
                        <img src="./media/airport.png" aria-hidden="true" />
                    </div>
                    <div className="right">
                        
                        <div className="lang-info">
                            <h1>{this.props.name}</h1>
                            <table className="table">
                                <tbody>
                                    <tr>
                                        <td className="l">{tr(lang, 'INF.NativeSpeakers')}</td>
                                        <td className="r">{this._formatPopulation(lang, this.props.population)}</td>
                                    </tr>
                                    <tr>
                                        <td className="l">{tr(lang, 'INF.LanguageFamily')}</td>
                                        <td className="r">{this.props.family}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p className="description">{this.props.description}</p>
                        </div>
                    </div>
                </div>
               <BotNavBar lang={this.props.lang} nextFn={this._continue} />
            </div>
        )
    }
});

var ConnectedInfo = connect(
    (state) => {
        let question = state.questions[state.game.currentQuestionId];
        let language = state.languages[question.target.langId];
        return {
            lang: state.lang,
            name: language['name_'+state.lang],
            family: language['family_'+state.lang],
            population: language.pop,
            iso: language.iso,
            description: language.description,
            
            questionId: state.game.currentQuestionId,
            nQuestions: state.questions.length,
        };
    },
    { changeQuestion, changeGameStage }
)(Info);


// Home page is skipped in final version, we go straight to Intro
let Home = React.createClass({
    propTypes: {
        changePage: PropTypes.func.isRequired,
    },

    render: function() {
        return (
            <div id="container">
                <div className="main">
                    <div className="content home">
                        <h1>Home</h1>
                        <p className="main-text">
                            Home
                        </p>
                    </div>
                    <BotNavBar lang={this.props.lang} nextFn={()=>{this.props.changePage(PAGES.GAME)}} />
                </div>
            </div>
        );
    }
});

// Intro page, the start / landing page of the game with story and instructions
let Intro = React.createClass({
    propTypes: {
        lang: PropTypes.string.isRequired,
        changeGameStage: PropTypes.func.isRequired,
    },

    render: function() {
        let lang = this.props.lang;
        return (
            <div className="main">
                <div className="content intro divided">
                    <div className="left">
                        <img src="./media/airport.png" aria-hidden="true" />
                    </div>
                    <div className="right">
                        <div className="top">
                            <h1>{tr(lang, 'INT.Title')}!</h1>
                            <p className="main-text">
                                {tr(lang, 'INT.Story')}
                                <br/><br/>
                                {tr(lang, 'INT.Instruction')}
                            </p>
                        </div>
                    </div>
                </div>
                <BotNavBar lang={this.props.lang} nextFn={()=>{this.props.changeGameStage(GAME_STAGES.QUESTION)}} />
            </div>
        );
    }
});


// The ending page of the game with score and statistics
let Ending = React.createClass({
    propTypes: {
        lang: PropTypes.string.isRequired,
        roundsCorrect: PropTypes.number.isRequired,
        roundsAnswered: PropTypes.number.isRequired,
        changePage: PropTypes.func.isRequired,
        playEndSound: PropTypes.func.isRequired,
    },

    componentDidMount: function() {
        // Play ending sound effect onload (audio autoplay doesn't work on mobile browsers)
        this.props.playEndSound();
    },

    render: function() {
        let lang = this.props.lang;
        let percentage = Math.round((this.props.roundsCorrect/this.props.roundsAnswered)*100);

        return (
            <div className="main">
                <div className="content ending">
                    <h1>{tr(lang, 'END.Finished')}</h1>
                    <p className="score-intro">{tr(lang, 'END.Score')}</p>
                    <p className="score">{percentage}%</p>
                    <p className="bye">
                        {tr(lang, 'END.Thanks')}<br/>
                        {tr(lang, 'END.PlayAgain')}<br/>
                        <br/>
                        <span className="visit">{tr(lang, 'END.AlsoVisit')}</span><br/>
                        <span className="url">apps.languageininteraction.nl</span>
                    </p>
                </div>
                <BotNavBar lang={this.props.lang} nextFn={()=>{this.props.changePage(PAGES.GAME)}}/>
            </div>
        );
    }
});

var ConnectedEnding = connect(
    (state) => {
        return {
            lang: state.lang,
            roundsCorrect: state.game.nCorrect,
            roundsAnswered: state.game.nAnswered,
        };
    },
    { changePage }
)(Ending);


// The actual game content, contains different content depending on stage
var Game = React.createClass({
    propTypes: {
        lang: PropTypes.string.isRequired,
        stage: PropTypes.string.isRequired,
        changeGameStage: PropTypes.func.isRequired,
    },

    // We have audio players in this component so that audio files can be cached and player instantly
    _playSound: function(ref) {
        if (this.refs[ref]) {
            this.refs[ref].play();
        }
    },

    render: function() {
        let main;
        switch (this.props.stage) {
            case GAME_STAGES.INTRO:
                main = <Intro lang={this.props.lang} changeGameStage={this.props.changeGameStage} />; break;
            case GAME_STAGES.QUESTION:
                main = <ConnectedQuestion />; break;
            case GAME_STAGES.ANSWER:
                main = <ConnectedAnswer 
                            playGoodSound={this._playSound.bind(this, "audioPlayerGood")} 
                            playBadSound={this._playSound.bind(this, "audioPlayerBad")} />;
                break;
            case GAME_STAGES.INFO:
                main = <ConnectedInfo />; break;
            case GAME_STAGES.ENDING:
                main = <ConnectedEnding playEndSound={this._playSound.bind(this, "audioPlayerEnd")} />; break;
        }

        return (
            <div id="container">
                <ConnectedProgressBar />
                {main}
                <audio ref="audioPlayerGood" src={SOUNDS.GOOD} />
                <audio ref="audioPlayerBad" src={SOUNDS.BAD} />
                <audio ref="audioPlayerEnd" src={SOUNDS.END} />
            </div>
        )
    }
});

var ConnectedGame = connect(
    (state) => {
        return {
            lang: state.lang,
            stage: state.game.stage,
        }
    },
    { changeGameStage }
)(Game);

// About page, is considered to be outside the flow of the game content 
let About = React.createClass({
    propTypes: {
        lang: PropTypes.string.isRequired,
        changePage: PropTypes.func.isRequired,
    },

    render: function() {
        let lang = this.props.lang;
        let title = tr(lang, 'ABT.Title');
        let pAboutThis = tr(lang, 'ABT.AboutThisHtml');
        let pAboutOther = tr(lang, 'ABT.AboutOtherHtml');
        let pCredits = tr(lang, 'ABT.AboutCreditsHtml');
        
        return (
            <div id="container">
                <div className="main">
                    <div className="content about">
                        <h1>{title}</h1>
                        <img className="logo" alt="Logo" src="./media/logo-lii.png" />
                        <p dangerouslySetInnerHTML={{__html:pAboutThis}}></p>
                        <p dangerouslySetInnerHTML={{__html:pAboutOther}}></p>
                        <p className="promo">apps.languageininteraction.nl</p>
                        <p className="smaller" dangerouslySetInnerHTML={{__html:pCredits}}></p>
                    </div>
                    <BotNavBar lang={this.props.lang} nextFn={()=>{this.props.changePage(PAGES.GAME)}}/>
                </div>
            </div>
        );
    }
})



var App = React.createClass({
    propTypes: {
        lang: PropTypes.string.isRequired,
        page: PropTypes.string.isRequired,
        changePage: PropTypes.func.isRequired,
    },

    render: function() {
        let content;
        switch (this.props.page) {
            case PAGES.GAME:
                content = <ConnectedGame />; break;
            case PAGES.SETTINGS:
                content = <div id="container" className="">Settings</div>; break; // Settings not implemented
            case PAGES.ABOUT:
                content = <About lang={this.props.lang} changePage={this.props.changePage} />; break;
            default:
                content = <Home changePage={this.props.changePage} />; break; // Home is not used, we start at GAME
        }

        return (
            <div id="app" aria-live="assertive" >
                <Toolbar />
                {content}
            </div>
        );
    }
});

var ConnectedApp = connect(
    (state) => {
        return {
            lang: state.lang,
            page: state.page,
        }
    },
    { changePage }
)(App);


export default ConnectedApp;


