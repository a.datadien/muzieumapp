

export const PAGES = Object.freeze({
    HOME: 'home',
    GAME: 'game',
    SETTINGS: 'settings',
    ABOUT: 'about',
});

export const AT = Object.freeze({
    CLICKED: 'CLICKED',
    PAGE_CHANGED: 'PAGE_CHANGED',
    QUESTION_ANSWERED: 'QUESTION_ANSWERED',
    SCORE_CHANGED: 'SCORE_CHANGED',
    //QUESTION_ENDED: 'QUESTION_ENDED',
    QUESTION_CHANGED: 'QUESTION_CHANGED', 
    GAME_STAGE_CHANGED: 'GAME_STAGE_CHANGED',
    LANG_CHANGED: 'LANG_CHANGED',
});

export const GAME_STAGES = Object.freeze({
    INTRO: 'INTRO',
    QUESTION: 'QUESTION',
    ANSWER: 'ANSWER',
    INFO: 'INFO',
    ENDING: 'ENDING',
});

export const LANGS = Object.freeze({
    NL: 'NL',
    EN: 'EN',
});
