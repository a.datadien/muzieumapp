
import _ from 'lodash';

import { PAGES, AT, GAME_STAGES, LANGS } from './constants';


import languages from '../resources/languages.json';
import descriptions from '../resources/languagedescriptions.json';

_.forOwn(languages, function(language, key) {
    language.description = descriptions[key].description;
});


const clipNames = Object.freeze(["1", "2", "3", "4", "5"]); // Names of the audio files for each language (without extension)
const nQuestions = 3;   // Questions per game
const nOptions = 3;     // Options per questions (currently the components only support 3)


function generateTarget(languages) {
    return {
        langId: _.sample(Object.keys(languages)),
        clipName: _.sample(clipNames),
    };
}

function generateOption(languages, target, isCorrect) {
    var langId, clipName;

    if (isCorrect) {
        // langId should match, clipName should not match 
        langId = target.langId;
        clipName = _.sample(_.without(clipNames, target.clipName));
    } else {
        // langId should not match, clipName can be anything
        langId = _.sample(_.without(Object.keys(languages), target.langId)),
        clipName = _.sample(clipNames);
    }
    
    return {
        langId: langId,
        clipName: clipName,
    }
}

function generateQuestions(languages, nQuestions, nOptions) {
    var questions = [];
    for (var i=0; i<nQuestions; i++) {
        var question = {
            id: i,
            target: generateTarget(languages),
            options: [],
            chosenOptionId: null,
        }

        var correctOptionId = _.random(0, nOptions-1);
        for (var j=0; j<nOptions; j++) {
            var option = generateOption(languages, question.target, j === correctOptionId);
            question.options.push(option);
        }

        questions.push(question);
    }
    return questions;
}






function question(state, action) {
    switch (action.type) {
        case AT.QUESTION_ANSWERED:
            if (action.questionId === state.id) {
                return { ...state, chosenOptionId:action.optionId }; //Object.assign({}, state, {chosenClip:action.answer});
            } else {
                return state;
            }
        default:
            return state;
    }
}

function questions(state, action) {
    if (state === undefined) {
        state = generateQuestions(languages, nQuestions, nOptions);
    }

    switch (action.type) {
        case AT.QUESTION_ANSWERED:
            return state.map((q)=>question(q, action));
        case AT.PAGE_CHANGED:
            if (action.newPage == PAGES.GAME) {
                return generateQuestions(languages, nQuestions, nOptions);
            } else {
                return state;
            }
        default:
            return state;
    }
}





let initialGame = {
    stage: GAME_STAGES.INTRO,
    currentQuestionId: 0,
    nAnswered: 0,
    nCorrect: 0,
}

function game(state = initialGame, action) {
    switch (action.type) {
        case AT.GAME_STAGE_CHANGED:
            return {...state, stage: action.newStage};
        case AT.QUESTION_CHANGED:
            return {...state, currentQuestionId:action.newQuestionId};
        case AT.PAGE_CHANGED:
            return initialGame;
        case AT.QUESTION_ANSWERED:
            if (action.isCorrect) {
                return {...state, nAnswered: state.nAnswered+1, nCorrect: state.nCorrect+1};
            } else {
                return {...state, nAnswered: state.nAnswered+1};
            }
        default:
            return state;
    }
}

function page(state = PAGES.GAME, action) {
    switch (action.type) {
        case AT.PAGE_CHANGED:
            return action.newPage; // todo: check validity?
        default:
            return state;
    }
}

function lang(state = LANGS.NL, action) {
    switch (action.type) {
        case AT.LANG_CHANGED:
            return action.newLang; // todo: check validity?
        default:
            return state;
    }
}


function rootReducer(state = {}, action) {
    //console.log('Reducer action: ', action);

    return {
        lang: lang(state.lang, action),
        page: page(state.page, action),
        game: game(state.game, action),
        questions: questions(state.questions, action),
        languages: languages,
    }
}

export default rootReducer;