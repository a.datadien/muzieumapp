
import { PAGES, AT } from './constants';








function validPage(page) {
    for (var propertyName in PAGES) {
        if (PAGES[propertyName] === page) {
            return true;
        }
    }
    return false;
}

export function changePage(newPage) {
    return function(dispatch, getState) {
        if (validPage(newPage)) {
            console.log('valid');
            dispatch({
                type: AT.PAGE_CHANGED,
                newPage: newPage,
            });
        } else{
            console.log('invalid page');
        }
    }
}


export function answerQuestion(questionId, optionId) {
    return function(dispatch, getState) {
        let state = getState();
        let question = state.questions[state.game.currentQuestionId];

        dispatch({
            type: AT.QUESTION_ANSWERED,
            questionId: questionId,
            optionId: optionId,
            isCorrect: question.target.langId === question.options[optionId].langId,
        });

        if (question.target.lang === question.options[optionId].lang) {
            dispatch({
                type: AT.SCORE_CHANGED,
                scoreChange: 1,
            });
        }
    }
}

export function endQuestion(questionId) {
    return function(dispatch, getState) {
        
        
        let state = getState();
        if (state.game.currentQuestionId == 1) {
            dispatch(changePage(PAGES.HOME));
        }

        dispatch({
            type: AT.QUESTION_ENDED,
            questionId: questionId,
        });
        
    }
}

export function changeQuestion(newQuestionId) {
    return {
        type: AT.QUESTION_CHANGED,
        newQuestionId: newQuestionId,
    }
}

export function changeGameStage(newStage) {
    return {
        type: AT.GAME_STAGE_CHANGED,
        newStage: newStage,
    }
}

// Change user interface language
export function changeLang(newLang) {
    return {
        type: AT.LANG_CHANGED,
        newLang: newLang,
    }
}