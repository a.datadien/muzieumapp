
import _ from 'lodash';

let dict = {};

dict.NL = {
    //Toolbar
    'TB.Home': 'Begin',
    'TB.About': 'Over dit spel',
    'TB.Settings': 'Instellingen',
    'TB.Introduction': 'Introductie',
    'TB.Round': 'Ronde',
    'TB.Answer': 'Antwoord',
    'TB.Info': 'Info',
    'TB.End': 'Einde',
    'TB.HomeBtn': 'Terug naar begin',
    'TB.ChangeLanguageBtn': 'Verander taal',
    'TB.AboutBtn': 'Achtergrondinformatie',

    //Intro
    'INT.Title': 'Welkom',
    'INT.Story': `
        Je werkt bij de klantenservice op een groot vliegveld.
        Het is jouw taak om reizigers te helpen die hun vrienden verloren zijn.
        Je begrijpt misschien niet wat ze zeggen, maar je weet wel dat de reizigers dezelfde taal spreken als hun vrienden.`,
    'INT.Instruction': `
        Luister goed naar de mensen op het vliegveld en probeer diegenen te vinden die dezelfde taal spreken.`,
    
    //Progressbar
    'PB.Answered': 'Beantwoord',
    'PB.Correct': 'Goed',

    //Question
    'Q.Title': 'Een reiziger vraagt om hulp',
    'Q.Instruction': `
        De reiziger zoekt zijn of haar vriend.
        Luister naar de reiziger en de mensen in de buurt.
        Probeer dan de persoon te vinden die dezelfde taal spreekt als de reiziger.`,

    //Audiocollection
    'AC.Choose': 'Kies',
    'AC.ChooseX': 'Kies {x}',
    'AC.Traveler': 'Reiziger',
    'AC.PersonX': 'Persoon {x}',
    'AC.ListenToX': 'Luister naar {x}',
    'AC.ListenToXSpeakingY': 'Luister naar {x} die {y} spreekt',

    //Answer
    'ANS.Correct': 'Goed gedaan!',
    'ANS.Incorrect': 'Helaas!',
    'ANS.CorrectExplain': `
        Zowel persoon {chosenLetter} als de reiziger spreken {correctLang}. Dankzij jou hebben ze elkaar gevonden!
        Luister opnieuw naar de mensen, of ga verder en ontdek meer over deze taal.`,
    'ANS.IncorrectExplain': `
        Persoon {chosenLetter} spreekt {chosenLang}, maar de reiziger spreekt {correctLang}.
        Luister opnieuw naar de mensen, of ga verder en ontdek meer over deze taal.`,

    //BotNavBar
    'BOT.Back': 'Terug',
    'BOT.Continue': 'Ga verder',

    //Info
    'INF.NativeSpeakers': 'Moedertaal van',
    'INF.LanguageFamily': 'Taalfamilie',
    'INF.thousand': 'duizend',
    'INF.million': 'miljoen',

    //Ending
    'END.Finished': 'Spel afgerond',
    'END.Score': 'Je score:',
    'END.Thanks': 'Bedankt voor het spelen!',
    'END.PlayAgain': 'Ga verder om opnieuw te spelen.',
    'END.AlsoVisit': 'Voor meer leuke taalspelletjes, bezoek:',

    //About
    'ABT.Title': 'Over LingQuest Light',
    'ABT.AboutThisHtml': `
        Dit spel is een lichtgewicht versie van een spel genaamd <b>LingQuest</b> dat is ontwikkeld voor het <b>Language in Interaction</b> project.
        Deze versie is ontworpen om bruikbaar te zijn voor blinden en slechtzienden en is oorspronkelijk ontwikkeld om tentoongesteld te worden in het muZIEum in Nijmegen.`,
    'ABT.AboutOtherHtml': `
        Probeer ook eens het originele LingQuest spel om nog meer exotische talen te ontdekken, gratis beschikbaar op de Apple App Store.
        Voor vragen en feedback kun je mailen naar info@languageininteraction.nl. 
        Meer informatie en andere <b>leuke taalspelletjes</b> zijn beschikbaar op de Language in Interaction website:`,
    'ABT.AboutCreditsHtml': `
        Dit spel is ontwikkeld door Arvind Datadien, supervisie door Pashiera Barkhuysen, getest door Michel van Breukelen.<br/>
        LingQuest is ontwikkeld door Peter Withers, Sean Roberts, Mark Dingemanse, Hedvig Skirgard, Pashiera Barkhuysen.<br/>
        Taalbeschrijvingen komen van Wikipedia (www.wikipedia.org), aangepast en vrijgegeven onder de CC BY-SA 3.0 licentie.`,

    '': '',
    '': '',

};

dict.EN = {
    //Toolbar
    'TB.Home': 'Home',
    'TB.About': 'About this game',
    'TB.Settings': 'Settings',
    'TB.Introduction': 'Introduction',
    'TB.Round': 'Round',
    'TB.Answer': 'Answer',
    'TB.Info': 'Info',
    'TB.End': 'End',
    'TB.HomeBtn': 'Home',
    'TB.ChangeLanguageBtn': 'Change Language',
    'TB.AboutBtn': 'About',

    //Intro
    'INT.Title': 'Welcome',
    'INT.Story': `
        You work at the service desk at a large airport.
        Your job is to help travelers find their missing companions.
        You may not understand what they're saying, but you know that each traveler speaks the same language as his or her companion.`,
    'INT.Instruction': `
        Listen carefully to the speech of people at the airport, and try to find the ones that speak the same language.`,
    
    //Progressbar
    'PB.Answered': 'Answered',
    'PB.Correct': 'Correct',

    //Question
    'Q.Title': 'A traveler approaches',
    'Q.Instruction': `
        The traveler has lost his or her companion.
        Listen to the traveler and to the people nearby.
        Then try to choose the person that speaks the traveler's language.`,

    //Audiocollection
    'AC.Choose': 'Choose',
    'AC.ChooseX': 'Choose {x}',
    'AC.Traveler': 'Traveler',
    'AC.PersonX': 'Person {x}',
    'AC.ListenToX': 'Listen to {x}',
    'AC.ListenToXSpeakingY': 'Listen to {x} speaking {y}',

    //Answer
    'ANS.Correct': 'Correct!',
    'ANS.Incorrect': 'Incorrect!',
    'ANS.CorrectExplain': `
        Both person {chosenLetter} and the traveler speak {correctLang}. Thanks to you they are reunited! 
        Listen again to the people, or continue for more information about the traveler's language.`,
    'ANS.IncorrectExplain': `
        Person {chosenLetter} speaks {chosenLang}, but the traveler speaks {correctLang}.
        Listen again to the people, or continue for more information about the traveler's language.`,

    //BotNavBar
    'BOT.Back': 'Back',
    'BOT.Continue': 'Continue',

    //Info
    'INF.NativeSpeakers': 'Native speakers',
    'INF.LanguageFamily': 'Language family',
    'INF.thousand': 'thousand',
    'INF.million': 'million',

    //Ending
    'END.Finished': 'Game finished',
    'END.Score': 'Your score:',
    'END.Thanks': 'Thanks for playing!',
    'END.PlayAgain': 'Press continue to play again.',
    'END.AlsoVisit': 'For more fun language games, visit:',

    //About
    'ABT.Title': 'About LingQuest Light',
    'ABT.AboutThisHtml': `
        This game is a lightweight version of a game called <b>LingQuest</b> which was developed by the <b>Language in Interaction</b> project.
        This version was designed to be useful for the visually impaired and was created for display in the muZIEum in Nijmegen.`,
    'ABT.AboutOtherHtml': `
        If you would like to explore even more exotic languages, download the original LingQuest game for free from the Apple App Store.
        For questions and feedback, please email info@languageininteraction.nl.
        More information and other <b>fun language games</b> can also be found on the Language in Interaction website:`,
    'ABT.AboutCreditsHtml': `
        This game was created by Arvind Datadien, supervised by Pashiera Barkhuysen, tested by Michel van Breukelen.<br/>
        LingQuest was created by Peter Withers, Sean Roberts, Mark Dingemanse, Hedvig Skirgard, Pashiera Barkhuysen.<br/>
        Language descriptions are from Wikipedia (www.wikipedia.org), modified and released under the CC BY-SA 3.0 License.`,

    '': '',
    '': '',
};

function fillParams(str, params = {}) {
    _.forOwn(params, function(value, key) {
        str = str.replace('{'+key+'}', value);
    });
    return str;
}


function tr(lang, strId, params = {}) {
    if (!dict[lang] || typeof dict[lang] !== 'object') {
        return '{language unknown}';
    }

    if (dict[lang][strId] !== undefined) {
        let result = dict[lang][strId];
        result = fillParams(result, params);
        return result;
    } else {
        return '{?'+strId+'?}';
    }
}

export { tr };

