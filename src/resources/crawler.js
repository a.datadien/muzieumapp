
console.log('STARTIN...');

var fs = require('fs');
var _ = require('lodash');
var request = require('request');
var cheerio = require('cheerio');

var languages = require('./languages.json');



function getDescription(iso, name, url) {
    return new Promise(function(resolve, reject) {
        request(url, function (error, response, body) {
            if (!error) {
                var $ = cheerio.load(body);

                var introElement = $('#mw-content-text > p').first();
                var descriptionRaw = introElement.html();

                introElement.find('.reference').remove();
                var description = introElement.text();
                //var re = new RegExp('('+name+')(.*?)(is a.*)', 'i');
                //description = description.replace(re, "$1 $3");

                var re = new RegExp('('+name+')(.*?)(is a.*|is the.*)', 'i');
                description = description.replace(re, "$1 $3");

                var result = {
                    //descriptionRaw: descriptionRaw,
                    description: description,
                };
                
                resolve(result);
            } else {
                reject(error);
            }
        });
    });
}



var baseUrl = 'https://en.wikipedia.org/wiki/';
var data = {};
var p = Promise.resolve({});
var i = 0;
var max = null;

_.forOwn(languages, function(val, key) {
    if (max && i > max) {
        return;
    } else {
        i++;
    }

    var iso = val.iso;
    var name = val.name;
    var url = baseUrl + name + '_language';

    p = p
    .then(function() {
        return getDescription(iso, name, url);
    })
    .then(function(result) {
        console.log('Success: '+iso+", chars:"+result.description.length)
        data[iso] = result;
    })
    .catch(function(error) {
        console.log('Error: '+iso, error);
    });
});

p.then(function(result) {
    console.log('Finished downloading. Now writing to file.');
    fs.writeFileSync("test.json", JSON.stringify(data));
})





/*
Removed data:
  "ces": {
     "iso":"ces"
    ,"name":"Czech"
    ,"pop":10619340
    ,"area":"Europe"
    ,"family":"Indo-European"
    ,"dobes":"0"
  },
 */


