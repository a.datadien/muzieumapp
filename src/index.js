
import React from 'react';
import ReactDOM from 'react-dom';

require('./index.html');
require('./styles/style.less');
require('./styles/font-awesome-4.6.3/less/font-awesome.less');

import App from './components/app.jsx';
import rootReducer from './common/reducers';

import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';


let store = createStore(rootReducer, applyMiddleware(thunk));

store.subscribe(() => {
    //console.log('Store changed:', store.getState());
});

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('app-container')
);

document.body.addEventListener('touchmove', function(event) {
    //event.preventDefault();
});